import { createBoard } from './board.js';
import './components/minesweeper-cell.js';
import './components/minesweeper-grid.js';
import { children, dataset, delegate, h, mount, props, style } from './h.js';
/** @typedef {import('./components/minesweeper-cell.js').default} MinesweeperCell */

const board = createBoard({ width: 30, height: 16, mines: 99 });

const cells = [];

for (const [x, column] of board.entries()) {
  for (const y of column.keys()) {
    cells.push(h(
      'minesweeper-cell',
      props({ state: 'hidden' }),
      dataset({
        x: String(x),
        y: String(y),
      }),
    ));
  }
}

/**
 *
 * @param {MinesweeperCell} cell
 */
function clearCell(cell) {
  const x = Number(cell.dataset.x);
  const y = Number(cell.dataset.y);
  const square = board[x][y];
  if (square.mine) {
    grid.querySelectorAll('minesweeper-cell').forEach(cell => {
      const x = Number(cell.dataset.x);
      const y = Number(cell.dataset.y);
      const square = board[x][y];
      cell.neighbours = square.neighbourMines;
      cell.state = square.mine ? 'mine' : 'clear';
    })
  } else {
    clearSquareAndNeighboors(x, y, square, cell);
  }
}

const grid = h(
  'minesweeper-grid',
  props({ width: 30, height: 16 }),
  children(...cells),
  delegate('minesweeper-cell[state="hidden"]', 'click', e => {
    /** @type {MinesweeperCell} */
    const cell = (e.target);
    clearCell(cell);
  }),
  delegate('minesweeper-cell[state="hidden"], minesweeper-cell[state="flagged"]', 'contextmenu', e => {
    e.preventDefault();
    /** @type {MinesweeperCell} */
    const cell = (e.target);
    if (cell.state === 'hidden') cell.state = 'flagged';
    else cell.state = 'hidden';
  }),
  delegate('minesweeper-cell[state="clear"]', 'click', e => {
    /** @type {MinesweeperCell} */
    const cell = (e.target);
    const { neighbours } = cell;
    if (neighbours === 0) return;

    const x = Number(cell.dataset.x), y = Number(cell.dataset.y);

    const neighourSelector = [-1, 0, 1].flatMap(
      xd => [-1, 0, 1].map(yd => `minesweeper-cell[data-x="${x + xd}"][data-y="${y + yd}"]`)
    ).join(',');

    /** @type {MinesweeperCell[]} */
    const neighourCells = ([...document.querySelectorAll(neighourSelector)]);

    const neighbourFlags = [...neighourCells].filter(cell => cell.state === 'flagged');

    if (neighbourFlags.length === neighbours) {
      neighourCells.filter(c => c.state === 'hidden').forEach(clearCell);
    }
  }),
);

/**
 * @param {number} x
 * @param {number} y
 * @param {typeof board[number][number]} square
 * @param {MinesweeperCell} cell
 */
function clearSquareAndNeighboors(x, y, square, cell) {
  if (cell.state !== 'hidden') return;
  cell.neighbours = square.neighbourMines;
  cell.state = 'clear';
  if (square.neighbourMines > 0) return;

  [-1, 0, 1].forEach(xd => [-1, 0, 1].forEach(yd => {
    const xn = x + xd, yn = y + yd;
    if (xn >= 0 && xn < board.length && yn >= 0 && yn < board[xn].length) {
      clearSquareAndNeighboors(
        xn,
        yn,
        board[xn][yn],
        /** @type {MinesweeperCell} */(grid.querySelector(`minesweeper-cell[data-x="${xn}"][data-y="${yn}"]`)),
      );
    }
  }));
}

mount(
  document.body,
  children(grid),
  style({ gridTemplateRows: '99vh', gridTemplateColumns: '100vw', margin: '0', display: 'grid' })
);
