import { relative } from '../common.js';
import { children, h, mount, props } from '../h.js';
import './scalable-text.js';

export default class MinesweeperCell extends HTMLElement {
  static get observedAttributes() {
    return ['state', 'neighbours'];
  }
  constructor() {
    super();

    /** @type {HTMLButtonElement} */
    const button = h(
      'button',
      children(h('scalable-text', children(String(normalizeNeighbours(this.getAttribute('neighbours'))))))
    );
    this._button = button
    mount(this.attachShadow({ mode: 'closed' }), children(
      h('link', props({ rel: 'stylesheet', href: relative(import.meta.url, 'minesweeper-cell.css') })),
      button,
    ));
  }

  get state() {
    return normalizeState(this.getAttribute('state'));
  }

  set state(value) {
    this.setAttribute('state', normalizeState(value));
  }

  get neighbours() {
    return normalizeNeighbours(this.getAttribute('neighbours'));
  }

  set neighbours(value) {
    const valueAsNumber = Number(value);
    if (Number.isInteger(valueAsNumber) && valueAsNumber >= 0 && valueAsNumber < 9)
      this.setAttribute('neighbours', String(valueAsNumber));
  }

  /**
   *
   * @param {string} name
   * @param {string | null} oldValue
   * @param {string | null} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue === newValue) return;
    if (name === 'neighbours') {
      this._button.children[0].textContent = String(normalizeNeighbours(newValue));
    } else if (name === 'state') {
      switch (normalizeState(newValue)) {
        case 'clear':
          const mines = this.neighbours;
          if (new Intl.PluralRules().select(mines) === 'other') this._button.title = `${mines} neighbour mines`;
          else this._button.title = `1 neighbour mine`;
          break;
        case 'flagged':
          this._button.title = 'Flagged';
          break;
        case 'mine':
          this._button.title = 'Mine';
          break;
        case 'hidden':
          this._button.title = 'Unknown';
          break;
      }
    }
  }
}

/**
 * @typedef {'hidden' | 'clear' | 'mine' | 'flagged'} MinesweeperCellState
 */

/**
 * @param {string | null} state
 * @returns {MinesweeperCellState}
 */
function normalizeState(state) {
  switch (state) {
    case 'clear':
    case 'flagged':
    case 'hidden':
    case 'mine':
      return state;
    default:
      return 'hidden';
  }
}

/**
 * @param {string | null} neighbours
 * @returns {number}
 */
function normalizeNeighbours(neighbours) {
  const value = Number(neighbours);
  if (Number.isInteger(value) && value >= 0 && value < 9) return value;
  return 0;
}

customElements.define('minesweeper-cell', MinesweeperCell);
