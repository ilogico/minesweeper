interface HTMLElementTagNameMap {
  'scalable-text': import('./scalable-text.js').default;
  'minesweeper-cell': import('./minesweeper-cell.js').default;
  'minesweeper-grid': import('./minesweeper-grid.js').default;
}
