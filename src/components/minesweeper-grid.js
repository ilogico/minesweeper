import { mount, children, h, props, style } from '../h.js';
import { relative } from '../common.js';

export default class MinesweeperGrid extends HTMLElement {
  static get observedAttributes() {
    return ['width', 'height'];
  }

  constructor() {
    super();
    const initialWidth = normalizeSize(this.getAttribute('width'));
    const initialHeight = normalizeSize(this.getAttribute('height'));

    /** @type {HTMLSlotElement} */
    const slot = h('slot', style({
      gridTemplateColumns: `repeat(${initialWidth}, 10rem)`,
      gridTemplateRows: `repeat(${initialHeight}, 10rem)`,
    }));

    this._slot = slot;

    mount(
      this.attachShadow({ mode: 'closed' }),
      children(
        h('link', props({ rel: 'stylesheet', href: relative(import.meta.url, 'minesweeper-grid.css') })),
        h('scalable-text', children(slot)),
      )
    );
  }

  get width() {
    return normalizeSize(this.getAttribute('width'));
  }

  set width(value) {
    const size = Number(value);
    if (Number.isInteger(value) && value > 0) this.setAttribute('width', String(size));
  }

  get height() {
    return normalizeSize(this.getAttribute('heigth'));
  }

  set height(value) {
    const size = Number(value);
    if (Number.isInteger(value) && value > 0) this.setAttribute('height', String(size));
  }

  /**
   * @param {string} name
   * @param {string | null} oldValue
   * @param {string | null} newValue
   */
  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue === newValue) return;
    if (name === 'width') {
      this._slot.style.gridTemplateColumns = `repeat(${normalizeSize(newValue)}, 10rem)`;
    } else if (name === 'height') {
      this._slot.style.gridTemplateRows = `repeat(${normalizeSize(newValue)}, 10rem)`;
    }
  }
}

/**
 * @param {string | null} size
 */
function normalizeSize(size) {
  const value = Number(size);
  if (Number.isInteger(value) && value > 0) return value;
  return 1;
}

customElements.define('minesweeper-grid', MinesweeperGrid);
