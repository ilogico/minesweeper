import { relative } from '../common.js';
import { children, h, mount, props, s, attributes } from '../h.js';

export default class ScalableText extends HTMLElement {
  constructor() {
    super();

    /** @type {SVGSVGElement} */
    let svg;

    mount(this.attachShadow({ mode: 'closed' }), children(
      h('link', props({ rel: 'stylesheet', href: relative(import.meta.url, 'scalable-text.css') })),
      s('svg', attributes(['viewBox', '0 0 1 1']), ref => svg = ref, children(
        s(
          'foreignObject',
          attributes(["width", "100%"], ["height", "100%"]),
          children(
            h('slot', slot => {
              new ResizeObserver(entries => {
                const { width, height } = entries[entries.length - 1].contentRect;
                svg.setAttribute('viewBox', `0 0 ${width} ${height}`);
              }).observe(slot);
            }),
          ),
        ),
      )),
    ));
  }
}

customElements.define('scalable-text', ScalableText);
