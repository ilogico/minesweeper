import { initialize } from './common.js';

/**
 * @template {Node} N
 * @typedef {import('./common.js').Initializer<N>} NodeInitializer
 */

/**
 * @template {keyof HTMLElementTagNameMap} N
 * @param {N} tag
 * @param  {...NodeInitializer<HTMLElementTagNameMap[N]>} initializations
 */
export function h(tag, ...initializations) {
  const node = document.createElement(tag);
  initialize(node, initializations);
  return node;
}

/**
 * @template {keyof SVGElementTagNameMap} N
 * @param {N} tag
 * @param  {...NodeInitializer<SVGElementTagNameMap[N]>} initializations
 */
export function s(tag, ...initializations) {
  const node = document.createElementNS("http://www.w3.org/2000/svg", tag);
  initialize(node, initializations);
  return node;
}

/**
 * @template {Node} N
 * @template {Partial<N>} P
 * @param {P} props
 * @returns {NodeInitializer<N>}
 */
export function props(props) {
  return (node) => {
    Object.assign(node, props);
  };
}

/**
 * @template {Node} N
 * @template {keyof HTMLElementEventMap} E
 * @param {E} event
 * @param {(this: N, event: HTMLElementEventMap[E]) => void} listener
 * @param {AddEventListenerOptions} [options]
 * @returns {NodeInitializer<N>}
 */
export function listen(event, listener, options) {
  return (node) =>
    node.addEventListener(
      event,
      /** @type {EventListener} **/ (listener),
      options,
    );
}

/**
 * @template {Node} N
 * @template {keyof HTMLElementEventMap} E
 * @param {string} query
 * @param {E} event
 * @param {(this: N, event: HTMLElementEventMap[E]) => void} listener
 * @param {AddEventListenerOptions} [options]
 * @returns {NodeInitializer<N>}
 */
export function delegate(query, event, listener, options) {
  return (node) =>
    node.addEventListener(
      event,
      event => {
        if (event.target instanceof Element && event.target.matches(query)) {
          listener.call(node, /** @type {HTMLElementEventMap[E]} */ (event));
        }
      },
      options,
    );
}

/**
 * @template {EventTarget} N
 * @param {{ [E in keyof HTMLElementEventMap]?: (this: N, event: HTMLElementEventMap[E]) => void}} handlers
 * @returns {NodeInitializer<N>}
 */
export function on(handlers) {
  const handlerList = Object.entries(handlers).filter(([, handler]) => handler);
  return (node) => {
    handlerList.forEach(([event, listener]) => {
      node.addEventListener(event, /** @type {EventListener} */ (listener));
    });
  };
}

/**
 * @param  {...[string, string]} attributes
 * @returns {NodeInitializer<Element>}
 */
export function attributes(...attributes) {
  return element => {
    attributes.forEach(([name, value]) => element.setAttribute(name, value));
  };
}

/**
 * @param {Partial<CSSStyleDeclaration>} styles
 * @returns {NodeInitializer<ElementCSSInlineStyle>}
 */
export function style(styles) {
  return element => {
    Object.assign(element.style, styles);
  }
}

/**
 * @param {Record<string, string>} data
 * @returns {NodeInitializer<HTMLOrSVGElement>}
 */
export function dataset(data) {
  return element => {
    Object.assign(element.dataset, data);
  }
}

/**
 * @template {Node} N
 * @param {N} root
 * @param  {...NodeInitializer<N>} initializers
 */
export function mount(root, ...initializers) {
  initialize(root, initializers);
}

/**
 * @param  {...(Node | string)} children
 * @returns {NodeInitializer<ParentNode>}
 */
export function children(...children) {
  return node => node.append(...children);
}

/**
 * @param {string} name
 * @returns {NodeInitializer<Element>}
 */
export function className(name) {
  return element => {
    element.className = name;
  }
}
