/**
 * @param {{ width: number, height: number, mines: number }} options
 */
export function createBoard({ width, height, mines }) {
    const total = width * height;
    const squares = [];

    for (let i = 0; i < mines; i++) {
        squares.push({ mine: true, neighbourMines: 0 });
    }

    for (let i = mines; i < total; i++) {
        squares.push({ mine: false, neighbourMines: 0 });
    }

    shuffle(squares);

    /** @type {typeof squares[]} */
    const columns = [];

    for (let i = 0; i < width; i++) {
        columns.push(squares.slice(i * height, (i + 1) * height));
    }

    columns.forEach((column, x) => column.forEach((square, y) => {
        if (square.mine) return;

        [0, 1, -1].forEach(horizontalOffset => [0, -1, 1].forEach(verticalOffset => {
            const x0 = x + horizontalOffset;
            const y0 = y + verticalOffset;
            if (x0 >= 0 && x0 < width && y0 >= 0 && y0 < height && columns[x0][y0].mine) square.neighbourMines++;
        }));
    }));

    return columns;
}

/** @param {unknown[]} array */
function shuffle(array) {
    for (let i = 1; i < array.length; i++) {
        const randomIndex = Math.random() * i + 1 | 0;
        [array[i], array[randomIndex]] = [array[randomIndex], array[i]];
    }
}
