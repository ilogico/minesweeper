interface ImportMeta {
  readonly url: string;
}

interface ResizeObserverEntry {
  readonly contentRect: DOMRectReadOnly;
  readonly target: Element;
}

class ResizeObserver {
  constructor(handler: (entries: ResizeObserverEntry[], observer: ResizeObserver) => void);
  observe(target: Element): void;
  unobserve(target: Element): void;
  disconnect(): void;
}
