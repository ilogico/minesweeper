/**
 * @template T
 * @callback Initializer
 * @param {T} target
 * @returns {void}
 */

/**
 * @template T
 * @param {T} target
 * @param {readonly Initializer<T>[]} initializations
 */
export function initialize(target, initializations) {
  initializations.forEach((init) => init(target));
}

/**
 * @param {string} path
 * @param {string} [file = '']
 */
export function relative(path, file = '') {
  return path.replace(/[^/]*$/, file);
}
